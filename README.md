# recipe-app-api-proxy

NGINX proxy app

## Usage

### Enviroment variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app forward requests to (default: `9000`)
